import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatTabsModule, MatNativeDateModule, MatToolbarModule, MatIconModule, MatCardModule, MatPaginatorModule } from '@angular/material';
import { MatTableModule } from '@angular/material/table'
import { MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatListModule} from '@angular/material/list';


import { AppComponent } from './app.component';
import { PresenterListComponent } from './presenter/presenter-list/presenter-list.component';
import { PresenterComponent } from './presenter/presenter.component';
import { AdminComponent } from './admin/admin.component';
import { AdminListComponent } from './admin/admin-list/admin-list.component';
import { PresenterDetailComponent } from './presenter/presenter-detail/presenter-detail.component';
import { PresenterHeaderComponent } from './presenter/presenter-header/presenter-header.component';
import { PresenterDetailHeaderComponent } from './presenter/presenter-detail-header/presenter-detail-header.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AppRoutingModule } from './app-routing.module';
import { PresentersService } from './services/presenters.service';
import { AdminAddComponent } from './admin/admin-add/admin-add.component';
import { AdminHeaderComponent } from './admin/admin-header/admin-header.component';
import { AdminDetailsComponent } from './admin/admin-details/admin-details.component';
import { AdminDetailsHeaderComponent } from './admin/admin-details-header/admin-details-header.component';
import { AdminAddHeaderComponent } from './admin/admin-add-header/admin-add-header.component';
import { FindPipe } from './find.pipe';
import { LoginComponent } from './login/login.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { AccountHeaderComponent } from './account-settings/account-header/account-header.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from './services/token-interceptor.service';



@NgModule({
  declarations: [
    AppComponent,
    PresenterListComponent,
    PresenterComponent,
    AdminComponent,
    AdminListComponent,
    PresenterDetailComponent,
    PresenterHeaderComponent,
    PresenterDetailHeaderComponent,
    PageNotFoundComponent,
    AdminAddComponent,
    AdminHeaderComponent,
    AdminDetailsComponent,
    AdminDetailsHeaderComponent,
    AdminAddHeaderComponent,
    FindPipe,
    LoginComponent,
    AccountSettingsComponent,
    AccountHeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    AppRoutingModule,
    MatTableModule,
    MatInputModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatToolbarModule,
    NgbModule,
    MatSortModule,
    MatIconModule,
    MatCardModule,
    HttpClientModule,
    MatPaginatorModule,
    MatListModule
  ],
  providers: [PresentersService, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
