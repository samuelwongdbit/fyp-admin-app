import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AdminsService } from '../services/admins.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  adminID;
  public loginform: FormGroup;
  admin = { email: "", password: "" };

  hide = true;

  constructor(private svcAdmin: AdminsService) { }

  ngOnInit() {
    this.loginform = new FormGroup({
      // 'username' : new FormControl(null, [Validators.required]),
      'password': new FormControl(null, [Validators.required]),
      'email': new FormControl(null, [Validators.required, Validators.email])
    })

  }
  public hasError = (controlName: string, errorName: string) => {
    return this.loginform.controls[controlName].hasError(errorName);
  }

  onLogin() {
    var email = this.loginform.value.email;
    var password = this.loginform.value.password;
    this.admin.email = email;
    this.admin.password = password;
    this.svcAdmin.loginAdmin(this.admin);
    this.svcAdmin.getEmail(this.admin.email);

    // let user = {
    //   email: this.loginform.value.email,
    //   password: this.loginform.value.password
    // }
    // this.svcAdmin.sendEmail(`http://localhost:3000/sendmail`, user).subscribe(
    //   data => {
    //     let res:any = data; 
    //     console.log(
    //       `👏 > 👏 > 👏 > 👏 ${user.email} is successfully register and mail has been sent and the message id is ${res.message} `
    //     );
    //   },
    //   err => {
    //     console.log(err);
    //   }
    // );
  }

}
