import { EventEmitter, Injectable } from '@angular/core';
import { Admin } from '../models/admin.model';
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})

export class AdminsService {
  adminAdded = new EventEmitter<void>();
  // adminUpdated = new EventEmitter<void>();
  dataSource = new MatTableDataSource();
  public adminList: Admin[];
  adminEmail;
  loggedIn = false;
  adminEmailID;
  result: string = this.makeString();
  constructor(private location: Location, private router: Router, public httpClient: HttpClient) { }

  // load admin list from database
  loadAdmin() {
    return this.httpClient.get<Admin[]>("http://localhost:3000/api/admin/list")
      .pipe(map((admins) => {
        this.adminList = admins;
        console.log('Admin loaded');
        return admins;
      },
        (err) => {
          console.log(err);
        })
      );
  }

  // get admin list
  getAdmin() {
    return this.adminList.slice();
  }

  // update profile info
  updateAdmin(adminInfo, oldPassword, newPassword) {
    console.log(adminInfo);
    // var encryptedOldPass = this.encrdecrService.set('123456$#@$^@1ERF', oldPassword);
    // var encryptedNewPass = this.encrdecrService.set('123456$#@$^@1ERF', newPassword);

    var admin = { adminid: adminInfo.adminid, email: adminInfo.email, name: adminInfo.name, oldPassword: oldPassword, newPassword: newPassword, status: adminInfo.status, profilepic: adminInfo.profilepic }

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    });

    const options = {
      headers: httpHeaders
    };

    var adminid = adminInfo.adminid;
    const url = `http://localhost:3000/api/admin/${adminid}/update`;

    this.httpClient.put<any>(url, { info: admin }, options).subscribe(
      res => {
        console.log(res);
        // this.presenterSelected = res;
        console.log("Successfully updated profile!")
        this.adminEmail = adminInfo.email;
        this.location.back();
      },
      err => {
        console.log(err);
        alert(err.error);
      });



    // console.log(adminInfo.adminid);

    // const httpHeaders = new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   'Cache-Control': 'no-cache'
    // });

    // const options = {
    //   headers: httpHeaders
    // };

    // const url = `http://localhost:3000/api/admin/${adminInfo.adminid}/update`;

    // this.httpClient.put<any>(url, { info: adminInfo }, options).subscribe(
    //   res => {
    //     console.log(res.message);
    //     for (var i = 0; i < this.adminList.length; i++) {
    //       if (this.adminList[i].adminid == adminInfo.adminid) {
    //         console.log(adminInfo);
    //         this.adminList.splice(i, 1, adminInfo);
    //       }
    //     }
    //     //set email to be used in getting admin details by email
    //     this.adminEmail = adminInfo.email;
    //     this.adminAdded.emit();
    //     this.router.navigate(['/adminList']);
    //   }, err => {
    //     console.log(err);
    //     alert(err.error);
    //   }
    // );
  }

  // block Admin
  blockAdmin(adminid) {
    var updateAdmin;

    for (let admin of this.adminList) {
      if (admin.adminid == adminid) {
        updateAdmin = admin;
      }
    }

    updateAdmin.status = 1;

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    });

    const options = {
      headers: httpHeaders
    };

    const url = `http://localhost:3000/api/admin/${adminid}/status/update`;


    this.httpClient.put<any>(url, { info: updateAdmin }, options).subscribe(
      res => {
        // for (let admin of this.adminList) {
        //   if (admin.adminid == adminid) {
        //     admin.status = 1;
        //   }
        // }
        this.adminAdded.emit();
        this.location.back();
        console.log('Succesfully block admin!');
      },
      err => {
        console.log(err);
        alert(err.error);
      }
    );
  }

  // unblock Admin
  unblockAdmin(adminid) {
    var updateAdmin;

    for (let admin of this.adminList) {
      if (admin.adminid == adminid) {
        updateAdmin = admin;
        // updateAdmin.status = 0;
      }
    }
    updateAdmin.status = 0;

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    });

    const options = {
      headers: httpHeaders
    };

    const url = `http://localhost:3000/api/admin/${adminid}/status/update`;

    this.httpClient.put<any>(url, { info: updateAdmin }, options).subscribe(
      res => {
        // for (let admin of this.adminList) {
        //   if (admin.adminid == adminid) {
        //     admin.status = 0;
        //   }
        // }
        this.adminAdded.emit();
        this.location.back();
        console.log('Succesfully unblock admin!');
      },
      err => {
        console.log(err);
        alert(err.error);
      }
    );
  }

  // delete Admin
  deleteAdmin(adminid) {
    var updateAdmin;

    for (let admin of this.adminList) {
      if (admin.adminid == adminid) {
        updateAdmin = admin;
        // updateAdmin.status = 2;
      }
    }
    updateAdmin.status = 2;

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    });

    const options = {
      headers: httpHeaders
    };

    const url = `http://localhost:3000/api/admin/${adminid}/status/update`;

    this.httpClient.put<any>(url, { info: updateAdmin }, options).subscribe(
      res => {
        // for (let admin of this.adminList) {
        //   if (admin.adminid == adminid) {
        //     admin.status = 2;
        //   }
        // }
        this.adminAdded.emit();
        this.location.back();
        console.log('Succesfully delete admin!');
      },
      err => {
        console.log(err);
        alert(err.error);
      }
    );
  }

  getAdminByID(id: number): Admin {
    return this.adminList.find(s => s.adminid === id);
  }

  getHeaderEmail() {
    return this.adminEmailID;
  }

  getEmail(id) {
    this.adminEmailID = id;
  }

  getAccount(email: string): Admin {
    return this.adminList.find(s => s.email.toLocaleLowerCase() === email.toLocaleLowerCase());
  }

  // add new admin
  registerAdmin(newAdminInfo) {
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    });

    const options = {
      headers: httpHeaders
    }

    this.httpClient.post<any>("http://localhost:3000/api/admin/register",
      { adminData: newAdminInfo }, options)
      .subscribe(
        res => {
          console.log(res.message);
          this.adminList.push(newAdminInfo);
          this.dataSource = new MatTableDataSource(newAdminInfo);
          this.adminAdded.emit();
          this.router.navigate(['/adminList']);
        },
        err => {
          console.log(err);
          alert(err.error);
        }
      )
  }

  // reset password for admin
  resetPasswordAdmin(id, newP) {
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    });

    const options = {
      headers: httpHeaders
    };

    const url = `http://localhost:3000/api/admin/resetpassword/${id}`;

    this.httpClient.put<any>(url, { info: newP }, options).subscribe(
      res => {
        console.log(res.message);
        this.router.navigate(['/login']);
      }, err => {
        console.log(err);
        alert(err.error);
      }
    );
  }

  logIn() {
    return !!localStorage.getItem('token');
  }

  // login admin
  loginAdmin(admin) {
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    });

    const options = {
      headers: httpHeaders
    }

    this.httpClient.post<any>("http://localhost:3000/api/admin/login",
      { adminData: admin }, options)
      .subscribe(
        res => {
          console.log(res);
          this.adminEmail = admin.email;
          localStorage.setItem('token', res.token)
          this.loggedIn = this.logIn();
          this.router.navigate(['/presenter']);
        },
        err => {
          console.log(err)
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              alert(err.error);
            }
          }
        }

      );
  }

  logout() {
    localStorage.removeItem('token');
    this.loggedIn = this.logIn();
    console.log('logged out');
    this.router.navigate(['/']);
  }

  isAuthenticated() {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.loggedIn)
      }, 1000);
    });
    return promise;
  }

  getToken() {
    return localStorage.getItem('token');
  }


  //nodemailer; for reset password via view details of admin
  sendEmail(id) {
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    });

    const options = {
      headers: httpHeaders
    }

    this.httpClient.post<any>("http://localhost:3000/api/admin/forgetpassword",
      { info: id }, options)
      .subscribe(
        data => {
          alert(
            `Email to reset password has been successfully sent to [${data.accepted}]`
          );
        },
        err => {
          console.log(err)
        }

      );
  }

  makeString(): string {
    let outString: string = '';
    let inOptions: string = 'abcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < 32; i++) {

      outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));

    }
    return outString;
  }

}


