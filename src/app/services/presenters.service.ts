import { Injectable, EventEmitter } from '@angular/core';
import { Presenter } from '../models/presenter.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class PresentersService {

  presAdded = new EventEmitter<void>();

  public presenterList: Presenter[];


  constructor(public location: Location, public httpClient: HttpClient) { }

  loadPresenter() {
    return this.httpClient.get<Presenter[]>("http://localhost:3000/api/presenter/list")
      .pipe(
        map((presenters) => {
          // display localize presenter created date time
          for (let presenter of presenters) {
            var startdt = (new Date(presenter.createdAt)).toString()
            var sdt = startdt.split(" ")
            var sdate = sdt[2]
            var smonth = sdt[1]
            var syear = sdt[3]
            var sdisplaydt = sdate + " " + smonth + " " + syear

            presenter.createdAt = sdisplaydt
          }
          
          this.presenterList = presenters;
          console.log("Presenter list loaded!")
          console.log(this.presenterList)
          return presenters;
        },
          (error) => { console.log(error) })
      );
  }

  getPresenter() {
    return this.presenterList.slice();
  }

  getPresenterByID(id: number): Presenter {
    return this.presenterList.find(s => s.presid === id);
  }

  blockPresenter(id) {
    var updatedData;

    for (let presenter of this.presenterList) {
      if (presenter.presid == id) {
        presenter.status = 1;
        updatedData = presenter;
      }
    }

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    });

    const options = {
      headers: httpHeaders
    }

    this.httpClient.put<Presenter>(`http://localhost:3000/api/presenter/${id}/status/update`,
      { presData: updatedData }, options)
      .subscribe(() => {
        this.presAdded.emit();
        this.location.back();
        console.log('Succesfully block presenter!');
      })
  }

  unblockPresenter(id) {
    var updatedData;

    for (let presenter of this.presenterList) {
      if (presenter.presid == id) {
        presenter.status = 0;
        updatedData = presenter;
      }
    }

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    });

    const options = {
      headers: httpHeaders
    }

    this.httpClient.put<Presenter>(`http://localhost:3000/api/presenter/${id}/status/update`,
      { presData: updatedData }, options)
      .subscribe(() => {
        this.presAdded.emit();
        this.location.back();
        console.log('Succesfully unblock presenter!');
      });
  }

  deletePresenter(id) {
    var updatedData;

    for (let presenter of this.presenterList) {
      if (presenter.presid == id) {
        presenter.status = 2;
        updatedData = presenter;
      }
    }

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    });

    const options = {
      headers: httpHeaders
    }

    this.httpClient.put<Presenter>(`http://localhost:3000/api/presenter/${id}/status/update`,
      { presData: updatedData }, options)
      .subscribe(() => {
        this.presAdded.emit();
        this.location.back();
        console.log('Succesfully delete presenter!');
      })
  }

}
