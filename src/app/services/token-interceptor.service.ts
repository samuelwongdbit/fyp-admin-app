import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from "@angular/common/http";
import { AdminsService } from './admins.service'

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private injecotr: Injector) { }

  intercept(req, next) {
    let adminSvc = this.injecotr.get(AdminsService);
    let tokenlizedReq = req.clone({
      setHeaders: {
        Authorization: `Bearer ${adminSvc.getToken()}`
      }
    });
    return next.handle(tokenlizedReq);
  }
}
