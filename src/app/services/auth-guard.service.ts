import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AdminsService} from "./admins.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router, private adminSvc: AdminsService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean> | Promise<boolean> | boolean {
      return this.adminSvc.isAuthenticated()
      .then((authenticated: boolean)=> {
        if(authenticated){
          return true;
        }
        else {
          console.log("Access Denied");
          this.router.navigate(['/']);
        }
      })
    }
  
}
