import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AdminsService } from 'src/app/services/admins.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Admin } from '../models/admin.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.css']
})
export class AccountSettingsComponent implements OnInit {

  public accountForm: FormGroup;

  hide = true;
  hide1 = true;
  hide2 = true;

  accountData: Admin;
  constructor(private location: Location, private adminSvc: AdminsService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.accountForm = new FormGroup({
      'name': new FormControl(null, [Validators.required]),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'oldpassword': new FormControl(null, [Validators.required]),
      'newpassword': new FormControl(null),
      'confirmpassword': new FormControl(null)

    })

    const email = this.route.snapshot.paramMap.get('id');
    this.accountData = this.adminSvc.getAccount(email);
    console.log(this.accountData);
    this.accountForm.controls['name'].setValue(this.accountData.name);
    this.accountForm.controls['email'].setValue(this.accountData.email);
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.accountForm.controls[controlName].hasError(errorName);
  }

  onLogout() {
    this.adminSvc.logout();
  }

  onUpdate() {
    var oldP = this.accountForm.value.oldpassword;
    var newP = this.accountForm.value.newpassword;
    var repeatP = this.accountForm.value.confirmpassword;

    if (newP == repeatP) {
      this.accountData.name = this.accountForm.value.name;
      this.accountData.email = this.accountForm.value.email;
      this.accountData.password = newP;
      this.adminSvc.updateAdmin(this.accountData, oldP, newP);
    }

    else {
      var inform = (<HTMLInputElement>document.getElementById('inform'))
      inform.innerHTML = "*Password entered does not match. Please enter again."
    }
  }

  goBack() {
    this.location.back();
  }
}
