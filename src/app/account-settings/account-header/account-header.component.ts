import { Component, OnInit } from '@angular/core';
import { AdminsService } from 'src/app/services/admins.service';
import { ActivatedRoute } from '@angular/router';
import { Admin } from '../../models/admin.model';
import { Location } from '@angular/common';


@Component({
  selector: 'app-account-header',
  templateUrl: './account-header.component.html',
  styleUrls: ['./account-header.component.css']
})
export class AccountHeaderComponent implements OnInit {
  accountData: Admin;
  constructor(private location: Location, private adminSvc: AdminsService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    var numid = parseInt(id)
    this.accountData = this.adminSvc.getAdminByID(numid);
  }

  goBack() {
    this.location.back();
  }
}
