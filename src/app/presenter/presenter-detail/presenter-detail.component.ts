import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PresentersService } from 'src/app/services/presenters.service';
import { Presenter } from '../../models/presenter.model';

@Component({
  selector: 'app-presenter-detail',
  templateUrl: './presenter-detail.component.html',
  styleUrls: ['./presenter-detail.component.css']
})
export class PresenterDetailComponent implements OnInit {

  presData: Presenter;

  constructor(private route: ActivatedRoute, private presSvc: PresentersService) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    var numid = parseInt(id)
    this.presData = this.presSvc.getPresenterByID(numid);
  }

  blockPresenter() {
    var id = this.route.snapshot.paramMap.get('id');
    var numid = parseInt(id);
    this.presSvc.blockPresenter(numid);
  }

  deletePresenter() {
    const id = this.route.snapshot.paramMap.get('id');
    var numid = parseInt(id);
    this.presSvc.deletePresenter(numid);
  }

  unblockPresenter(){
    var id = this.route.snapshot.paramMap.get('id');
    var numid = parseInt(id);
    this.presSvc.unblockPresenter(numid);
  }

}
