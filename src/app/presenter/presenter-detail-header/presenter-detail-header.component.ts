import { Component, OnInit } from '@angular/core';
import { AdminsService} from '../../services/admins.service';

@Component({
  selector: 'app-presenter-detail-header',
  templateUrl: './presenter-detail-header.component.html',
  styleUrls: ['./presenter-detail-header.component.css']
})
export class PresenterDetailHeaderComponent implements OnInit {
  adminEmail;
  
  constructor(private adminSvc: AdminsService) { }

  ngOnInit() {
    this.adminEmail = this.adminSvc.getHeaderEmail();

  }

}
