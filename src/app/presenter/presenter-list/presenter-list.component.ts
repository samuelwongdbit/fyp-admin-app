import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Presenter } from '../../models/presenter.model';
// import { PresentersService } from ''
import { PresentersService } from '../../services/presenters.service';
import { AdminsService } from '../../services/admins.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';



@Component({
  selector: 'app-presenter-list',
  templateUrl: './presenter-list.component.html',
  styleUrls: ['./presenter-list.component.css']
})
export class PresenterListComponent implements OnInit {
  @Input() myPresenter: Presenter[];
  @Input() presenterBlocked: Presenter[];
  @ViewChild('sortActive', { static: true }) sortActive: MatSort;
  @ViewChild('sortBlocked', { static: true }) sortBlocked: MatSort;
  @ViewChild('paginatorActive', { static: true }) paginatorActive: MatPaginator;
  @ViewChild('paginatorBlocked', { static: true }) paginatorBlocked: MatPaginator;

  sortSelected;
  paginatorSelected;
  ELEMENT_DATA: Presenter[];
  dataSource;
  tabIndex = 0;
  displayedColumns = ['profilepic', 'name', 'email', 'createdAt', 'button'];

  constructor(private adminSvc: AdminsService, private presSvc: PresentersService, private router: Router) { }

  ngOnInit() {
    this.sortSelected = this.sortActive;
    this.paginatorSelected = this.paginatorActive;

    this.presSvc.loadPresenter()
      .subscribe(() => {
        this.ELEMENT_DATA = this.presSvc.getPresenter();
        var activePresenter = []

        for (let presenter of this.ELEMENT_DATA) {
          if (presenter.status == 0) {
            activePresenter.push(presenter)
          }
        }

        this.ELEMENT_DATA = activePresenter;
        this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
        this.dataSource.sort = this.sortSelected;
        this.dataSource.paginator = this.paginatorSelected;
        this.dataSource.sortingDataAccessor = (data, sortHeadId) => data[sortHeadId].toLocaleLowerCase();
      });

    this.presSvc.presAdded.subscribe(() => {
      this.ELEMENT_DATA = this.presSvc.getPresenter();
      var activePresenter = []

      for (let presenter of this.ELEMENT_DATA) {
        if (presenter.status == this.tabIndex) {
          activePresenter.push(presenter)
        }
      }

      this.ELEMENT_DATA = activePresenter;
      this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
      this.dataSource.sort = this.sortSelected;
      this.dataSource.paginator = this.paginatorSelected;
      this.dataSource.sortingDataAccessor = (data, sortHeadId) => data[sortHeadId].toLocaleLowerCase();
    });

    // load admin (to navigate from presenter list page to profile page)
    this.adminSvc.loadAdmin().subscribe();
  }

  onPresenterDetail() {
    this.router.navigate(['/presenterDetail']);
  }

  yourPresenter($event) {
    this.ELEMENT_DATA = this.presSvc.getPresenter();
    var filteredPresenter = [];

    if ($event.index == 0) {
      this.tabIndex = 0;
      this.sortSelected = this.sortActive;
      this.paginatorSelected = this.paginatorActive;

      for (let presenter of this.ELEMENT_DATA) {
        if (presenter.status == 0) {
          filteredPresenter.push(presenter)
        }
      }
    }
    else if ($event.index == 1) {
      this.tabIndex = 1;
      this.sortSelected = this.sortBlocked;
      this.paginatorSelected = this.paginatorBlocked;

      for (let presenter of this.ELEMENT_DATA) {
        if (presenter.status == 1) {
          filteredPresenter.push(presenter)
        }
      }
    }

    this.ELEMENT_DATA = filteredPresenter;
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.dataSource.sort = this.sortSelected;
    this.dataSource.paginator = this.paginatorSelected;
    this.dataSource.sortingDataAccessor = (data, sortHeadId) => data[sortHeadId].toLocaleLowerCase();
  }

  onUnblockPres(id) {
    this.presSvc.unblockPresenter(id);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filterPredicate = function (data: Presenter, filter: string): boolean {
      return data.name.toLowerCase().includes(filter)
    };

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
