import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Admin } from '../../models/admin.model';
import { AdminsService } from '../../services/admins.service';

@Component({
  selector: 'app-presenter-header',
  templateUrl: './presenter-header.component.html',
  styleUrls: ['./presenter-header.component.css']
})
export class PresenterHeaderComponent implements OnInit {
  adminEmail;
  
  constructor(private adminSvc: AdminsService) { }

  ngOnInit() {
    this.adminEmail = this.adminSvc.getHeaderEmail();
  }

}
