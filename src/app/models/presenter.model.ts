export class Presenter {
    public presid: number;
    public email: string;
    public name: string;
    public password: string;
    public status: number;
    public profilepic: string;
    public createdAt: string;

    constructor(presid: number, email: string, name: string, password: string, status: number, profilepic: string, createdAt: string) {
        this.presid = presid;
        this.email = email;
        this.name = name;
        this.password = password;
        this.status = status;
        this.profilepic = profilepic;
        this.createdAt = createdAt;
    }
}