export class Admin {
    public adminid: number;
    public name: string;
    public email: string;
    public password: string;
    public status: number;

    constructor(adminid: number, name: string, email: string, password: string, status: number) {
        this.adminid = adminid;
        this.name = name;
        this.email = email;
        this.password = password;
        this.status = status;
    }
}