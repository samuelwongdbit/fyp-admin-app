import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';
import { PresenterListComponent } from './presenter/presenter-list/presenter-list.component';
import { AdminComponent } from './admin/admin.component';
import { AdminListComponent } from './admin/admin-list/admin-list.component';
import { PresenterDetailComponent } from './presenter/presenter-detail/presenter-detail.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PresenterComponent } from './presenter/presenter.component';
import { AdminAddComponent } from './admin/admin-add/admin-add.component';
import { AdminDetailsComponent } from './admin/admin-details/admin-details.component';
import { AuthGuardService } from './services/auth-guard.service';
import { AccountSettingsComponent } from './account-settings/account-settings.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'presenter', canActivate: [AuthGuardService], component: PresenterComponent },
  { path: 'presenter/:id', canActivate: [AuthGuardService], component: PresenterDetailComponent },
  { path: 'adminList', canActivate: [AuthGuardService], component: AdminListComponent },
  { path: 'adminAdd', canActivate: [AuthGuardService], component: AdminAddComponent },
  { path: 'admin/:id', canActivate: [AuthGuardService], component: AdminDetailsComponent },
  { path: 'account/:id', canActivate: [AuthGuardService], component: AccountSettingsComponent },
  { path: '**', redirectTo: '/not-found' },
  { path: 'not-found', component: PageNotFoundComponent }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
