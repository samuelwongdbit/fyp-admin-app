import { Component, OnInit } from '@angular/core';
import { Admin } from '../models/admin.model';
import { AdminsService } from '../services/admins.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  providers: [AdminsService]
})
export class AdminComponent implements OnInit {

  adminList: Admin[] = [];
  adminBlocked: Admin[] = [];

  constructor(private adminSvc: AdminsService) { }

  ngOnInit() {
    this.adminList = this.adminSvc.getAdmin();
    this.adminSvc.adminAdded.subscribe(() => {
      this.adminList = this.adminSvc.getAdmin();
    })

    this.adminList = this.adminSvc.getAdmin();
  }


  onAdminAdded(newAdminInfo) {
    this.adminList.push(newAdminInfo);
  }


}
