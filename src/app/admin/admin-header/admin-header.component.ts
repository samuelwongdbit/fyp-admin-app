import { Component, OnInit } from '@angular/core';
import { AdminsService } from '../../services/admins.service';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.css']
})
export class AdminHeaderComponent implements OnInit {

  adminEmail;
  
  constructor(private adminSvc: AdminsService) { }

  ngOnInit() {
    this.adminEmail = this.adminSvc.getHeaderEmail();
  }

}
