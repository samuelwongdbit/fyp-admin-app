import { Component, OnInit } from '@angular/core';
import { AdminsService } from '../../services/admins.service';

@Component({
  selector: 'app-admin-add-header',
  templateUrl: './admin-add-header.component.html',
  styleUrls: ['./admin-add-header.component.css']
})
export class AdminAddHeaderComponent implements OnInit {
  adminEmail;
  
  constructor(private adminSvc: AdminsService) { }

  ngOnInit() {
    this.adminEmail = this.adminSvc.getHeaderEmail();

  }

}
