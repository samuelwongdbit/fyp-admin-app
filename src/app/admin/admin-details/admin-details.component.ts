import { Component, OnInit } from '@angular/core';
import { AdminsService } from 'src/app/services/admins.service';
import { ActivatedRoute } from '@angular/router';
import { Admin } from '../../models/admin.model';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { variable } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-admin-details',
  templateUrl: './admin-details.component.html',
  styleUrls: ['./admin-details.component.css']
})
export class AdminDetailsComponent implements OnInit {
  adminData: Admin;
  accountData: Admin [] = this.adminSvc.getAdmin();
  adminEmail;
  adminPassword;

  show: boolean = true;

  constructor(private adminSvc: AdminsService, private route: ActivatedRoute, public httpClient: HttpClient) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    var numid = parseInt(id)
    this.adminData = this.adminSvc.getAdminByID(numid);
  }


  deleteAdmin() {
    const id = this.route.snapshot.paramMap.get('id');
    var numid = parseInt(id);
    this.adminSvc.deleteAdmin(numid);
  }

  blockAdmin() {
    const id = this.route.snapshot.paramMap.get('id');
    var numid = parseInt(id);
    this.adminSvc.blockAdmin(numid);

  }

  unblockAdmin(){
    const id = this.route.snapshot.paramMap.get('id');
    var numid = parseInt(id);
    this.adminSvc.unblockAdmin(numid);
  }

  // onResetPassword(){
  //   // const email = this.route.snapshot.paramMap.get('id');
  //   // this.adminData = this.adminSvc.getAccount(email);
  //   // this.adminSvc.resetPassword(this.adminData);
  // }

  onResetPassword(){
    const id = this.route.snapshot.paramMap.get('id');
    var numid = parseInt(id)
    console.log(numid);
    this.adminSvc.sendEmail(numid);
  }
}
