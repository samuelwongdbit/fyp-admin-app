import { Component, OnInit } from '@angular/core';
import { AdminsService } from '../../services/admins.service';

@Component({
  selector: 'app-admin-details-header',
  templateUrl: './admin-details-header.component.html',
  styleUrls: ['./admin-details-header.component.css']
})
export class AdminDetailsHeaderComponent implements OnInit {

  adminEmail;
  
  constructor(private adminSvc: AdminsService) { }

  ngOnInit() {
    this.adminEmail = this.adminSvc.getHeaderEmail();

  }

}
