import { Component, OnInit } from '@angular/core';
import { Admin } from '../../models/admin.model';
import { AdminsService } from '../../services/admins.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin-add',
  templateUrl: './admin-add.component.html',
  styleUrls: ['./admin-add.component.css']
})
export class AdminAddComponent implements OnInit {
  inputInfo: Admin;
  public adminForm: FormGroup;

  constructor(private adminSvc: AdminsService) { }

  hide = true;

  ngOnInit() {
    this.adminForm = new FormGroup({
      'password': new FormControl(null, [Validators.required]),
      'name': new FormControl(null, [Validators.required]),
      'email': new FormControl(null, [Validators.required, Validators.email])
    })
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.adminForm.controls[controlName].hasError(errorName);
  }

  onAdd() {
    this.adminSvc.registerAdmin(new Admin(
      0,
      this.adminForm.value.name,
      this.adminForm.value.email,
      this.adminForm.value.password,
      0
    ));
  }

}