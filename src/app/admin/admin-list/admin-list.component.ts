import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Admin } from '../../models/admin.model';
import { AdminsService } from 'src/app/services/admins.service';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.css']
})
export class AdminListComponent implements OnInit {

  @Input() adminBlocked: Admin[];
  @ViewChild('sortActive', { static: true }) sortActive: MatSort;
  @ViewChild('sortBlocked', { static: true }) sortBlocked: MatSort;
  @ViewChild('paginatorActive', { static: true }) paginatorActive: MatPaginator;
  @ViewChild('paginatorBlocked', { static: true }) paginatorBlocked: MatPaginator;

  sortSelected;
  paginatorSelected;
  ELEMENT_DATA: Admin[];
  dataSource;
  tabIndex = 0;
  searchStr = "";
  displayedColumns: string[] = ['name', 'email', 'button'];

  constructor(private adminSvc: AdminsService) { }

  ngOnInit() {
    this.sortSelected = this.sortActive;
    this.paginatorSelected = this.paginatorActive;

    this.adminSvc.loadAdmin().subscribe(() => {
      this.ELEMENT_DATA = this.adminSvc.getAdmin();
      var activeAdmin = []

      for (let admin of this.ELEMENT_DATA) {
        if (admin.status == 0) {
          activeAdmin.push(admin)
        }
      }
      this.ELEMENT_DATA = activeAdmin;
      this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);

      this.dataSource.sort = this.sortSelected;
      this.dataSource.sortingDataAccessor = (data, sortHeadId) => data[sortHeadId].toLocaleLowerCase();
      this.dataSource.paginator = this.paginatorSelected;
    });


    this.adminSvc.adminAdded.subscribe(() => {
      this.ELEMENT_DATA = this.adminSvc.getAdmin();
      var activeAdmin = []

      for (let admin of this.ELEMENT_DATA) {
        if (admin.status == this.tabIndex) {
          activeAdmin.push(admin)
        }
      }
      this.ELEMENT_DATA = activeAdmin;
      this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);

      this.dataSource.sort = this.sortSelected;
      this.dataSource.sortingDataAccessor = (data, sortHeadId) => data[sortHeadId].toLocaleLowerCase();
      this.dataSource.paginator = this.paginatorSelected;
    });
  }

  yourAdmin($event) {
    this.tabIndex = $event.index;

    this.ELEMENT_DATA = this.adminSvc.getAdmin()
    var filteredAdmin = []

    if ($event.index == 0) {
      this.sortSelected = this.sortActive;
      this.paginatorSelected = this.paginatorActive;

      for (let admin of this.ELEMENT_DATA) {
        if (admin.status == 0) {
          filteredAdmin.push(admin)
        }
      }
    }
    else if ($event.index == 1) {
      this.sortSelected = this.sortBlocked;
      this.paginatorSelected = this.paginatorBlocked;

      for (let admin of this.ELEMENT_DATA) {
        if (admin.status == 1) {
          filteredAdmin.push(admin)
        }
      }
    }
    this.ELEMENT_DATA = filteredAdmin;
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);

    this.dataSource.sort = this.sortSelected;
    this.dataSource.sortingDataAccessor = (data, sortHeadId) => data[sortHeadId].toLocaleLowerCase();
    this.dataSource.paginator = this.paginatorSelected;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filterPredicate = function (data: Admin, filter: string): boolean {
      return data.name.toLowerCase().includes(filter)
    };

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  unblockAdmin(id) {
    this.adminSvc.unblockAdmin(id);
  }

}



